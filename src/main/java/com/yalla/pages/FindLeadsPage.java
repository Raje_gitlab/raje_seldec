package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{ 

	public FindLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFirstName;	
	@FindBy(how=How.XPATH, using="//span[text()='Phone']") WebElement elePhone;	
	@FindBy(how=How.XPATH, using="//input[@name='phoneNumber']") WebElement elePhoneNumber;	
	@FindBy(how=How.XPATH, using="//input[@name='id']") WebElement eleLeadId;	
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how=How.XPATH, using="(//table[@class='x-grid3-row-table']//td//a)[1]") WebElement eleFirstLead;
	@FindBy(how=How.XPATH, using="//div[@class='x-paging-info']") WebElement eleVerifyText;
	
	
	public FindLeadsPage enterFirstName(String fname) {
		clearAndType(eleFirstName, fname);  
		return this;
	}
	
	public FindLeadsPage enterLeadId(String leadId) {
		clearAndType(eleLeadId, leadId);  
		return this;
	}
	
	public FindLeadsPage clickPhone() {
		click(elePhone);  
		return this;
	}
	
	public FindLeadsPage enterPhoneNumber(String phoneNumber) {
		clearAndType(elePhoneNumber, phoneNumber);  
		return this;
	}
	
	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		return this;
	}
	
	public ViewLeadPage clickFirstLead() {
		click(eleFirstLead);
		return new ViewLeadPage();
	}
	
	
	public FindLeadsPage verifyNoRecordsDisplayed() {
		verifyPartialText(eleVerifyText, "No records to display");
		return this;
	}
	
}







