package com.yalla.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_DeleteLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_DeleteLead";
		testcaseDec = "Delete lead in leaftaps";
		author = "Rajeswari";
		category = "smoke";
		excelFileName = "TC004_DeleteLead";
	} 

	@Test(dataProvider="fetchData") 
	public void editLead(String uName, String pwd, String phoneNumber) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.clickPhone()
		.enterPhoneNumber(phoneNumber)
		.clickFindLeads()
		.clickFirstLead()
		.clickDelete()
		.clickFindLeads()
		.enterLeadId("10478")
		.clickFindLeads()
		.verifyNoRecordsDisplayed();
	}
}