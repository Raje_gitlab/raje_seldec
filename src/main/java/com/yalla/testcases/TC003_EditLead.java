package com.yalla.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Edit lead in leaftaps";
		author = "Rajeswari";
		category = "smoke";
		excelFileName = "TC003_EditLead";
	} 

	@Test(dataProvider="fetchData") 
	public void editLead(String uName, String pwd, String cName, String fName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(fName)
		.clickFindLeads()
		.clickFirstLead()
		.verifyTitle()
		.clickEdit()
		.enterCompanyName(cName)
		.clickUpdate()
		.verifyCompanyName(cName);
	}
}